clear all; 
%Using functions for the first time 
n = 50; %total populations number
num_inf = 1; %number of infected individuals
num_sus = n - num_inf; %number of suspectible individuals
infection_vec = []; %number of recovered individuals
% pop_Mat_N = zeros(n,4); %future matrix
% colNames = {'Individual','Infection_Status','x','y'};
% Pop_Table = array2table(pop_Mat,'VariableNames',colNames);
area_M = 10; % are of infected individuals
timesteps = 10; 
inf_rate = 0.3;
inf_radius = 1;
[pop_Mat_C] = matrix_creator(n,num_inf, area_M);
%% update matrix 
for i = 1:timesteps
    [num_sus,num_inf, pop_Mat_C] = infection(num_sus, num_inf,pop_Mat_C, inf_radius, inf_rate, n);
    [pop_Mat_C] = random_movement(pop_Mat_C, area_M,n);
%     inf_location = [];
%     sus_location = [];
%     for j = 1:n
%         if pop_Mat_C(j,4) == 2
%             inf_location = [inf_location, j];
%         else
%             sus_location = [sus_location, j];
%         end
%     end
%     sus_x = [];
%     sus_y = [];
%     inf_x = [];
%     inf_y = [];
%     for j =  1:length(sus_location)
%         sus_x = [sus_x, pop_Mat_C(sus_location(j),2)];
%         sus_y = [sus_y, pop_Mat_C(sus_location(j),3)];
%     end
%     for j = 1:length(inf_location)
%         inf_x = [inf_x, pop_Mat_C(inf_location(j),2)];
%         inf_y = [inf_y, pop_Mat_C(inf_location(j),3)];
%     end
%     hold on
%     plot(sus_x,sus_y, '.', 'markersize', 18, 'MarkerEdgeColor','b', 'MarkerFaceColor', 'b')
%     plot(inf_x,inf_y, '.', 'markersize', 18, 'MarkerEdgeColor','r', 'MarkerFaceColor', 'r')
%     hold off
   
end

