clear all; 
%Using functions for the first time 
n = 50; %total populations number
num_inf = 2; %number of infected individuals
num_sus = n - num_inf; %number of suspectible individuals
infection_vec = []; %number of recovered individuals
% pop_Mat_N = zeros(n,4); %future matrix
% colNames = {'Individual','Infection_Status','x','y'};
% Pop_Table = array2table(pop_Mat,'VariableNames',colNames);
area_M = 10; % are of infected individuals
timesteps = 50; 
inf_rate = 0.3;
inf_radius = 1;
[pop_Mat_C] = matrix_creator(n,num_inf, area_M);
%% update matrix 
[sus_x,sus_y, inf_x, inf_y] = animation_params(pop_Mat_C,n);

for i = 1:timesteps
    [num_sus,num_inf, pop_Mat_C] = infection(num_sus, num_inf,pop_Mat_C, inf_radius, inf_rate, n);
    [pop_Mat_C] = random_movement(pop_Mat_C, area_M,n);



end
