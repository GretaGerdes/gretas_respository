function [pop_Mat_C] = matrix_creator(n,num_inf)
%Creates a matrix containing population information
%% initializing the population matrix
% include ID and infection status 
age_v = [.5579,.236, 0.16524, 0.03958];
gender_v = [.5,.5];
global area_M  ID x_pos y_pos inf_status age_group gender inf_prob ;
pop_Mat_C = zeros(n,7);
for i = 1: num_inf 
    pop_Mat_C(i,ID) = round(i);
    pop_Mat_C(i,inf_status) = 2;
end
for i = num_inf+1:n
    pop_Mat_C(i,ID) = round(i);
    pop_Mat_C(i,inf_status) = 1;
end
%% add the x and y positions
for i = 1:n
    pop_Mat_C(i,x_pos) = (1+ (area_M-1)*rand());
    pop_Mat_C(i,y_pos) = (1+ (area_M-1)*rand());
end

% add demographics 
% add gender
gender_n = [];
for i = 1: length(gender_v)
    gender_n = [gender_n, round(gender_v(i)*n)];
end
% add age 
age_n = [];
for i = 1: length(age_v)
    age_n = [age_n, round(age_v(i)*n)];
end

%% fill in age information
% age < 35
for i = 1: age_n(1)
    pop_Mat_C(i,age_group) = 1;
end
% 35<= age <= 50
for i = age_n(1)+1: age_n(1)+ age_n(2)
    pop_Mat_C(i,age_group) = 2;
end
% 51<= age<= 75
for i = age_n(1)+age_n(2)+1 : age_n(1)+ age_n(2) + age_n(3)
    pop_Mat_C(i,age_group) = 3;
end
% age > 75
for i = age_n(1)+ age_n(2)+ age_n(3)+1 : n
    pop_Mat_C(i,age_group) = 4;
end 
%% fill in gender information
% create a vector : gender_vector that contains the numerical
% representation of males and females
gender_vector  = zeros(n,1);
for i = 1: gender_n(1)
     gender_vector(i) = 1;
end
% fill in males
for i = gender_n(1)+1: n
    gender_vector(i) = 2;
end
% randomly shuffle the males and females in the population 
gender_vector_2 = gender_vector(randperm(length(gender_vector)));

for i = 1:n 
    pop_Mat_C(i,gender) = gender_vector_2(i);
end
    


%% include infection probability
[prob] = probability_infection(pop_Mat_C,n);
for i = 1:n 
    pop_Mat_C(i,inf_prob) = prob(i);
end
    
    



