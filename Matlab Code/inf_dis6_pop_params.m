clear all; 
%Using functions for the first time 
n = 50; %total populations number
num_inf = 1; %number of infected individuals
num_sus = n - num_inf; %number of suspectible individuals
global area_M timesteps inf_rate inf_radius ID x_pos y_pos inf_status age_group gender inf_prob beta;
area_M = 10; % are of infected individuals
timesteps = 10; 
inf_rate = 0.3;
inf_radius = 1;
ID = 1;
x_pos = 2;
y_pos = 3;
inf_status = 4;
age_group = 5;
gender = 6;
inf_prob = 7;
beta = 0.3;

[pop_Mat_C] = matrix_creator(n,num_inf)
%% update matrix 

for i = 1:timesteps
    [num_sus,num_inf, pop_Mat_C] = infection(pop_Mat_C, n);
    [pop_Mat_C] = random_movement(pop_Mat_C,n);
    [sus_x,sus_y, inf_x, inf_y] = simulation_params(pop_Mat_C,n);
   

end

