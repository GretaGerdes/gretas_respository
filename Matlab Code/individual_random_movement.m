function [rx, ry] = individual_random_movement(i,pop_Mat_C, dimensions)
% randomly moves an individual in a random direction, and updates the
% location in the population matrix 
angle = rand()*2*pi; % movement angle in radians
global x_pos y_pos;
rx = pop_Mat_C(i,x_pos);
ry = pop_Mat_C(i, y_pos);
rx = rx + 0.3*cos(angle);
ry = ry + 0.3*sin(angle);



%% check to see if the individual is still in the common space
if rx < dimensions(1)
    rx = 2*dimensions(1)- rx;
elseif rx > dimensions(2)
    rx =  2*dimensions(2) - rx;
end

if ry < dimensions(3)
    ry = 2*dimensions(3) - ry;
elseif ry > dimensions(4)
     ry =  2*dimensions(4)- ry;
end


end 


