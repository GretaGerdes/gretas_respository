function [cx,cy] = find_path(position_vector, destination)
% finds the path between individual and the center of the common space
global d;
slope = (destination(2) - position_vector(2))/(destination(1) - position_vector(1));
syms b; 
b = solve(position_vector(2) == slope * position_vector(1) + b, b);
line = [slope,b];
syms x y;
eq1 = y == line(1)*x + line(2);
eq2 = d == sqrt((x - position_vector(1))^2 + (y-position_vector(2))^2);
[x,y] = solve([eq1,eq2], [x,y]);
if sqrt((x(1) - destination(1))^2 + (y(1)- destination(2))^2) < sqrt((x(2) - destination(1))^2 + (y(2)- destination(2))^2)
    cx = x(1) ;
    cy = y(1) ;
else
    cx= x(2);
    cy= y(2);
end

end

