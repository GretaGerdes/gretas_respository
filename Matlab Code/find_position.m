function [position] = find_position(i, pop_Mat_C)
% This function looks at the location of individuals and assigns them with
% a numeral representation of that position
% dorms = p

% hallways = 2
% common space = 3
global x_pos y_pos size_of_room common_space;
if pop_Mat_C(i, x_pos) <= size_of_room
    position = 1;
elseif common_space(1)<= pop_Mat_C(i, x_pos) && pop_Mat_C(i, x_pos) <= common_space(2) && common_space(3) <= pop_Mat_C(i, y_pos) && pop_Mat_C(i, y_pos) <= common_space(4)
    position = 3;
else 
    position = 2;
    
   
end


