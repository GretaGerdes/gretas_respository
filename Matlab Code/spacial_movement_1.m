function [pop_Mat_C] = spacial_movement_1(pop_Mat_C,n)
% moves individuals from dorms to common space
global x_pos y_pos prev_x prev_y prob_leaving_room prob_leaving_cs center_cs  common_space size_of_room location;


%% moving from dorm 
for i = 1:n
    position_vector = [pop_Mat_C(i,x_pos), pop_Mat_C(i,y_pos)];
    [position] = find_position(i, pop_Mat_C);
    pop_Mat_C(i,location) = position;
    if position == 1
        destination = center_cs;
        if rand < prob_leaving_room % in their dorm
            pop_Mat_C(i,prev_x) = pop_Mat_C(i, x_pos);
            pop_Mat_C(i,prev_y) = pop_Mat_C(i,y_pos);
            position_vector(1) = size_of_room + 0.1;
            pop_Mat_C(i, x_pos) = position_vector(1);
        end

    elseif  position == 2
        if pop_Mat_C(i, x_pos) == size_of_room + 0.1
            destination = center_cs;
            [cx,cy] = find_path(position_vector, destination);
            pop_Mat_C(i, x_pos) = cx;
            pop_Mat_C(i,y_pos) = cy;
            
        elseif sqrt((center_cs(1)-pop_Mat_C(i,prev_x))^2 + (center_cs(2)- pop_Mat_C(i,prev_y))^2) > sqrt((center_cs(1)- pop_Mat_C(i, x_pos))^2 + (center_cs(2)- pop_Mat_C(i,y_pos))^2) &&  pop_Mat_C(i, x_pos) ~= size_of_room + 0.1
            destination = center_cs;
            [cx,cy] = find_path(position_vector, destination);
            pop_Mat_C(i, x_pos) = cx;
            pop_Mat_C(i,y_pos) = cy;
        else
            destination = [0.5, i-0.5];
            [cx,cy] = find_path(position_vector, destination);
            pop_Mat_C(i, x_pos) = cx;
            pop_Mat_C(i,y_pos) = cy;
        end
    else 
        
        if rand() < prob_leaving_cs
            pop_Mat_C(i,prev_x) = pop_Mat_C(i, x_pos); 
            pop_Mat_C(i,prev_y) = pop_Mat_C(i, y_pos);
            position_vector(1) = 7.9;
            pop_Mat_C(i, x_pos) = position_vector(1);
            position_vector(2) = 5;
            pop_Mat_C(i, y_pos) = position_vector(2);
        else
            pop_Mat_C(i,prev_x) = pop_Mat_C(i, x_pos); 
            pop_Mat_C(i, prev_y) = pop_Mat_C(i, y_pos);
            dimensions= common_space;
            [rx, ry] = individual_random_movement(i,pop_Mat_C,dimensions);
            pop_Mat_C(i, x_pos) = rx;
            pop_Mat_C(i, y_pos) = ry;
        end 
    end
end
end