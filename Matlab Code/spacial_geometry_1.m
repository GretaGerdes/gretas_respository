clear all; 
%Using functions for the first time 
n = 10; %total populations number
num_inf = 1; %number of infected individuals
num_sus = n - num_inf; %number of suspectible individuals
global area_M timesteps inf_rate inf_radius ID x_pos y_pos inf_status age_group gender inf_prob beta prev_x prev_y location size_of_room prob_leaving_room prob_leaving_cs common_space center_cs d;
area_M = 10; % are of infected individuals
timesteps = 100000; 
inf_rate = 0.3;
inf_radius = 1;
ID = 1;
x_pos = 2;
y_pos = 3;
inf_status = 4;
age_group = 5;
gender = 6;
inf_prob = 7;
prev_x = 8;
prev_y = 9;
location = 10;
beta = 0.3;
size_of_room = 1;
prob_leaving_room = 0.01;
prob_leaving_cs = 0.01;
common_space = [8,10,4,6]; % x boundaries and y boundaries
center_cs = [9,5];
d = 0.2;
%%
[pop_Mat_C] = spacial_movement_matrix_creator_1(n,num_inf);
[sus_x,sus_y, inf_x, inf_y] = animation_params(pop_Mat_C,n);
                            % Box of complex
square = [0,0,area_M,area_M,0; 0,area_M,area_M,0,0]; 
figure(1)                    		% Open a new figure window
hold on 
% Coordinates for the corners
grid on                              
line(square(1,:),square(2,:));
% first animation window
p1 = plot(sus_x,sus_y, '.', 'markersize', 18, 'MarkerEdgeColor','b', 'MarkerFaceColor', 'b');
p2 = plot(inf_x,inf_y, '.', 'markersize', 18, 'MarkerEdgeColor','r', 'MarkerFaceColor', 'r');
axis manual
hold off

%% 
for i = 1: timesteps
    [num_sus,num_inf,pop_Mat_C] = infection(pop_Mat_C, n);
    [pop_Mat_C] = spacial_movement_1(pop_Mat_C,n);
    
    [sus_x,sus_y, inf_x, inf_y] = animation_params(pop_Mat_C,n);
    p1.XData = sus_x;
    p1.YData = sus_y;
    p2.XData = inf_x;
    p2.YData = inf_y;
    
    
    drawnow
end

